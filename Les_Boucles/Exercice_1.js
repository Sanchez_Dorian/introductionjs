
//initialisation de la variable n à 0;
var n = 0;

//Tant que n est different de 10;
while(n != 10)
{
  //On affiche n;
  console.log("n : ", n);
  //On incrémente n de 1
  n = n + 1;
}