
//Initialisation de la fonction ran();
function ran(var_min, var_max)
{
  //retourne la valeur arrondie inférieur de la mult //;
	return Math.floor((Math.random())*(var_max-var_min+1)+var_min);
}

//initialisation de la variable a à 0;
var a = 100;

//initialisation de la variable b ayant une valeur comprise entre 1 et 100 (call de la fonction ran);
var b = ran(1, 100);

//Si b est une valeur superieur ou égale à 1 et inferieure ou égale à 100;
if (b >=1 && b <= 100)
{
  //Tant que a est supérieur ou égale 10;
  while(a >= 10)
  {
    //resultat est égale au produit de a et b;
    resultat = a*b;
    //On affiche resultat;
    console.log("resultat a*b : ", resultat);
    //On décrémente a de 1
    a = a - 1;
  }
}