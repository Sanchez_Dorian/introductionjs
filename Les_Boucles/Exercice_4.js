
//initialisation de la variable a à 1;
var a = 1;

//Tant que a est different de 10;
while(a < 10)
{
  //On affiche a;
  console.log("a : ", a);
  //On incrémente a de la moitié de a;
  a = a + a/2;
}